package fr.ajc.spring.perroux.tpmvc;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import model.Entreprise;
import repo.EntrepriseRepository;

@Controller
public class UserController {
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Value("${spring.application.name}")
	String appName;
	
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		return "home";
	}
	
	@PostMapping("/subscribe")
	public String subscribe(@Valid @ModelAttribute("entreprise") Entreprise entreprise, BindingResult result, Model model, HttpServletResponse response) {
		new UserSubscribeValidator().validate(entreprise, result);
		if(result.hasErrors()) {
			System.out.println("L'entreprise n'est pas valide");
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return "subscribe";
		}
		
		System.out.println(entreprise);
		entrepriseRepository.save(entreprise);
		response.setStatus(HttpServletResponse.SC_CREATED);
		return "success";
	}
	
	@GetMapping("/subscribe")
	public String subscribe(Model model) {
		model.addAttribute("entreprise",new Entreprise());
		return "subscribe";
	}
	
	@ModelAttribute("entreprise")
	public Entreprise initEntreprise() {
		Entreprise entreprise = new Entreprise();
		return entreprise;
	}
}