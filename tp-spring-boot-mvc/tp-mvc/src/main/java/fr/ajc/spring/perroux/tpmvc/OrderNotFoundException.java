package fr.ajc.spring.perroux.tpmvc;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="SIRET INCONNU")  // 404
public class OrderNotFoundException extends RuntimeException {
	private String message;
	public OrderNotFoundException(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
