package fr.ajc.spring.perroux.tpmvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import model.Entreprise;
import model.EntrepriseAPI;
import repo.EntrepriseRepository;

@RestController
@RequestMapping("/api/users")
public class UserControllerAPI {

	@Autowired
	private EntrepriseRepository entrepriseRepository;

	/*@GetMapping("/{siret}")
	public Iterable<EntrepriseAPI> findBySiret(@PathVariable String siret) {
		List<Entreprise> entreprises = (List<Entreprise>) entrepriseRepository.findBySiret(siret);
		List<EntrepriseAPI> entreprisesAPI = new ArrayList<EntrepriseAPI>();
		for(Entreprise e : entreprises) {
			entreprisesAPI.add(new EntrepriseAPI(e));
		}
		
		if(entreprisesAPI.size() == 0) {
			throw new OrderNotFoundException("PAS DE SIRET TROUVE");
		}
		
		return entreprisesAPI;
	}*/
	
	@RequestMapping(
			value = "/{siret}",
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody Iterable<EntrepriseAPI> findBySiret(@PathVariable String siret) {
		List<Entreprise> entreprises = (List<Entreprise>) entrepriseRepository.findBySiret(siret);
		List<EntrepriseAPI> entreprisesAPI = new ArrayList<EntrepriseAPI>();
		for(Entreprise e : entreprises) {
			entreprisesAPI.add(new EntrepriseAPI(e));
		}
		
		if(entreprisesAPI.size() == 0) {
			throw new OrderNotFoundException("PAS DE SIRET TROUVE");
		}
		
		return entreprisesAPI;
	}
	
	
	@RequestMapping(
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody Iterable<EntrepriseAPI> findAll(@RequestParam(value = "sort", required= false) String sort, @RequestParam(value = "order", required = false) String order) {
		
		List<Entreprise> entreprises = new ArrayList<Entreprise>();
		if(sort != null && order != null) {
			if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("DATE")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByDateInscriptionAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("DATE")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByDateInscriptionDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("NAME")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByRaisonSocialAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("NAME")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByRaisonSocialDesc();
			}
			else {
				entreprises = (List<Entreprise>) entrepriseRepository.findAll();
			}
		}else {
			entreprises = (List<Entreprise>) entrepriseRepository.findAll();
		}
		List<EntrepriseAPI> entreprisesAPI = new ArrayList<EntrepriseAPI>();
		for(Entreprise e : entreprises) {
			entreprisesAPI.add(new EntrepriseAPI(e));
		}
		return entreprisesAPI;
	}

	/*@GetMapping
	public Iterable<EntrepriseAPI> findAll(@RequestParam(value = "sort", required= false) String sort, @RequestParam(value = "order", required = false) String order) {
		
		List<Entreprise> entreprises = new ArrayList<Entreprise>();
		if(sort != null && order != null) {
			if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("DATE")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByDateInscriptionAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("DATE")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByDateInscriptionDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("NAME")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByRaisonSocialAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("NAME")) {
				entreprises = (List<Entreprise>) entrepriseRepository.findAllByOrderByRaisonSocialDesc();
			}
			else {
				entreprises = (List<Entreprise>) entrepriseRepository.findAll();
			}
		}else {
			entreprises = (List<Entreprise>) entrepriseRepository.findAll();
		}
		List<EntrepriseAPI> entreprisesAPI = new ArrayList<EntrepriseAPI>();
		for(Entreprise e : entreprises) {
			entreprisesAPI.add(new EntrepriseAPI(e));
		}
		return entreprisesAPI;
	}*/

	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Entreprise create(Entreprise entreprise) {
		return entrepriseRepository.save(entreprise);
	}
	
}