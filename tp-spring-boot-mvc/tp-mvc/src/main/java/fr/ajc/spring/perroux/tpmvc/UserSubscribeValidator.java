package fr.ajc.spring.perroux.tpmvc;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import model.Entreprise;

public class UserSubscribeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Entreprise.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Entreprise entreprise = (Entreprise) target;
		if(!entreprise.getTvaIntracom().equals("")) {
			if(entreprise.getTvaIntracom().length()!= 13) {
				errors.rejectValue("tvaIntracom", "too.short");
			}else {
				String firstC = entreprise.getTvaIntracom().substring(0, 2);
				if(firstC.toUpperCase().equals("FR")) {
					int cle = Integer.parseInt(entreprise.getTvaIntracom().substring(2, 4));
					System.out.println(cle);
					int chiffres = Integer.parseInt(entreprise.getTvaIntracom().substring(4, entreprise.getTvaIntracom().length()));
					System.out.println(chiffres);
					if(entreprise.getSiret().length()>=9) {
						int siren = Integer.parseInt(entreprise.getSiret().substring(0, 9));
						System.out.println(siren);
						
						if(siren != chiffres) {
							errors.rejectValue("tvaIntracom", "different.siren");
						}else {
							int cletva = (12+3*(siren%97))%97;
							System.out.println("clé tva "+cletva);
							if(cletva!=cle) {
								errors.rejectValue("tvaIntracom", "invalid.cletva");
							}
						}
					}else {
						errors.rejectValue("tvaIntracom", "different.siren");
					}
				}else {
					errors.rejectValue("tvaIntracom", "invalid.locate");
				}
			}
		}
	}

}
