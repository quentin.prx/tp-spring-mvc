package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.Entreprise;

public interface EntrepriseRepository extends CrudRepository<Entreprise, Long> {
	List<Entreprise> findAllByOrderByRaisonSocialAsc();
	List<Entreprise> findAllByOrderByRaisonSocialDesc();
	List<Entreprise> findAllByOrderByDateInscriptionAsc();
	List<Entreprise> findAllByOrderByDateInscriptionDesc();
	List<Entreprise> findBySiret(String siret);
}
