package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


@Entity
public class Entreprise {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotEmpty(message = "L''email est obligatoire")
	@Email(message = "L''email n'est pas valide")
	private String email;
	
	@NotEmpty(message = "La mot de passe est obligatoire")
	@Pattern(regexp="^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,}$",message="Le mot de passe doit contenir au minimum un lettre Majuscule, une lettre minuscule, un chiffre. 6 caractères minimum")  
	private String password;
	
	@NotEmpty(message = "La raison social est obligatoire")
	private String raisonSocial;
	
	private String tvaIntracom;
	
	@NotEmpty(message = "Le numéro de siret est obligatoire")
	@Column(unique = true)
	@Pattern(regexp="^([0-9]{14,14})$", message = "Le numéro de siret n''est pas valide")
	private String siret;
	
	@Min(value=5, message="Le nombre minimum de salarié est de 5")
	private Integer nombreEmploye;
	
	private boolean newsletter;
	
	private Date dateInscription = new Date();
	
	public Entreprise() {
	}
	
	public Entreprise(String email, String password, String raisonSocial, String tvaIntracom, String siret,
			Integer nombreEmploye, boolean newsletter) {
		this.email = email;
		this.password = password;
		this.raisonSocial = raisonSocial;
		this.tvaIntracom = tvaIntracom;
		this.siret = siret;
		this.nombreEmploye = nombreEmploye;
		this.newsletter = newsletter;
		this.dateInscription = new Date();
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	public String getTvaIntracom() {
		return tvaIntracom;
	}

	public void setTvaIntracom(String tvaIntracom) {
		this.tvaIntracom = tvaIntracom;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public Integer getNombreEmploye() {
		return nombreEmploye;
	}

	public void setNombreEmploye(Integer nombreEmploye) {
		this.nombreEmploye = nombreEmploye;
	}

	public boolean isNewsletter() {
		return newsletter;
	}
	
	

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}
	
	public EntrepriseAPI getEntrepriseAPI() {
		return new EntrepriseAPI(raisonSocial, tvaIntracom, siret, newsletter);
	}
	

	@Override
	public String toString() {
		return "Entreprise [id=" + id + ", email=" + email + ", password=" + password + ", raisonSocial=" + raisonSocial
				+ ", tvaIntracom=" + tvaIntracom + ", siret=" + siret + ", nombreEmploye=" + nombreEmploye
				+ ", newsletter=" + newsletter + ", dateInscription=" + dateInscription + "]";
	}
	
}
