package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;



public class EntrepriseAPI {
	private long id;
	private String raisonSocial;
	private String tvaIntracom;
	private String siret;
	private Integer nombreEmploye;
	private boolean newsletter;	
	private Date dateInscription = new Date();
	
	public EntrepriseAPI() {
	}
	
	public EntrepriseAPI(Entreprise e) {
		this.id = e.getId();
		this.raisonSocial = e.getRaisonSocial();
		this.tvaIntracom = e.getTvaIntracom();
		this.siret = e.getSiret();
		this.nombreEmploye = e.getNombreEmploye();
		this.newsletter = e.isNewsletter();
		this.dateInscription = e.getDateInscription();
	}
	
	public EntrepriseAPI(String raisonSocial, String tvaIntracom, String siret,
			Integer nombreEmploye, boolean newsletter) {
		this.raisonSocial = raisonSocial;
		this.tvaIntracom = tvaIntracom;
		this.siret = siret;
		this.nombreEmploye = nombreEmploye;
		this.newsletter = newsletter;
		this.dateInscription = new Date();
	}
	
	public EntrepriseAPI(String raisonSocial, String tvaIntracom, String siret,
			boolean newsletter) {
		this.raisonSocial = raisonSocial;
		this.tvaIntracom = tvaIntracom;
		this.siret = siret;
		this.newsletter = newsletter;
		this.dateInscription = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	public String getTvaIntracom() {
		return tvaIntracom;
	}

	public void setTvaIntracom(String tvaIntracom) {
		this.tvaIntracom = tvaIntracom;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public Integer getNombreEmploye() {
		return nombreEmploye;
	}

	public void setNombreEmploye(Integer nombreEmploye) {
		this.nombreEmploye = nombreEmploye;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	@Override
	public String toString() {
		return "EntrepriseAPI [id=" + id + ", raisonSocial=" + raisonSocial + ", tvaIntracom=" + tvaIntracom
				+ ", siret=" + siret + ", nombreEmploye=" + nombreEmploye + ", newsletter=" + newsletter
				+ ", dateInscription=" + dateInscription + "]";
	}
	
}
